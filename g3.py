class CGPA:
    def __init__(self, marks, grades, credit_units):
        self.marks = marks
        self.grades = grades
        self.credit_units = credit_units

    @staticmethod
    def get_grades(marks):
        theGrades = []
        for mark in marks:
            if mark >= 90:
                theGrades.append("A+")
            elif mark >= 80:
                theGrades.append("A")
            elif mark >= 75:
                theGrades.append("B+")
            elif mark >= 70:
                theGrades.append("B")
            elif mark >= 65:
                theGrades.append("C+")
            elif mark >= 60:
                theGrades.append("C")
            elif mark >= 55:
                theGrades.append("D+")
            elif mark >= 50:
                theGrades.append("D")
            elif mark >= 45:
                theGrades.append("E")
            elif mark >= 40:
                theGrades.append("E-")
            else:
                theGrades.append("F")
        return theGrades


class BasicCalculator:
    def basic_calculator(self, operator, values):
        total = 0
        if operator == "+":
            total = sum(values)
        elif operator == "-":
            result = values[0]
            for value in values[1:]:
                result -= value
            total = result
        elif operator == "*":
            result = values[0]
            for value in values[1:]:
                result *= value
            total = result
        elif operator == "/":
            result = values[0]
            for value in values[1:]:
                if value != 0:
                    result /= value
            total = result
        else:
            print("Invalid operation")
        return total


class CGPACalculator(CGPA, BasicCalculator):
    def calculate_cgpa(self):
        grade_points = {
            "A+": 5.0,
            "A": 5.0,
            "B+": 4.5,
            "B": 4.0,
            "C+": 3.5,
            "C": 3.0,
            "D+": 2.5,
            "D": 2.0,
            "E": 1.5,
            "E-": 1.0,
            "F": 0.0,
        }
        total_credit_units = sum(self.credit_units)
        weighted_sum = sum(
            [
                grade_points[grade] * credit_units[i]
                for i, grade in enumerate(self.grades)
            ]
        )
        cgpa = weighted_sum / total_credit_units
        return cgpa


student_numbers = [2300711585, 2300712369, 2300724496, 2300726712]

# Calculate the sum of student numbers
sum_of_student_numbers = sum(student_numbers)
# Extract values from the sum by removing the extreme value '8' on the left
marks = [
    int(str(sum_of_student_numbers)[1:3]),
    int(str(sum_of_student_numbers)[3:5]),
    int(str(sum_of_student_numbers)[5:7]),
    int(str(sum_of_student_numbers)[7:9]),
]


grades = CGPA.get_grades(marks)
credit_units = [4, 4, 4, 4]
cgpa = CGPACalculator(marks, grades, credit_units)

the_cgpa = cgpa.calculate_cgpa()
with open("CGPA.doc", "a") as word_document:
    word_document.write("CGPA using classes.\n")
    word_document.write(f"The CGPA is: {the_cgpa}\n")
